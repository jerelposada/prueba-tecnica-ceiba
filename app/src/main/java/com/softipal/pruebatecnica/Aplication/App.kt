package com.softipal.pruebatecnica.Aplication

import android.app.Application
import com.softipal.pruebatecnica.repository.database.UsersDatabase

class App : Application() {



    companion object{
        private var DB: UsersDatabase? = null
        fun getDB():UsersDatabase {
            return DB!!
        }
    }


    override fun onCreate() {
        super.onCreate()
        DB = UsersDatabase.getDB(applicationContext)
    }

}