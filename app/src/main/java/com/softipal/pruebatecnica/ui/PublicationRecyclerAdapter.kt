package com.softipal.pruebatecnica.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.softipal.pruebatecnica.R
import com.softipal.pruebatecnica.databinding.ListPublicationBinding
import com.softipal.pruebatecnica.repository.database.entities.Publications


class PublicationRecyclerAdapter(private val context: Context, val listPublication: List<Publications>): RecyclerView.Adapter<ViewHolder<*>>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<*> {
        return PublicationViewHolder(LayoutInflater.from(context).inflate(R.layout.list_publication,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder<*>, position: Int) {
       when(holder){
           is PublicationViewHolder -> holder.bin(listPublication[position],position)
       }
    }

    override fun getItemCount(): Int {
      return listPublication.size
    }

    inner class PublicationViewHolder(itemView: View): ViewHolder<Publications>(itemView){

        val binding = ListPublicationBinding.bind(itemView)

        override fun bin(item: Publications, position: Int) {
            binding.publication.text = item.body
        }

    }
}



