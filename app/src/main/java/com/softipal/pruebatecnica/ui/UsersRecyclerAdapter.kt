package com.softipal.pruebatecnica.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.softipal.pruebatecnica.R
import com.softipal.pruebatecnica.databinding.ListUserBinding
import com.softipal.pruebatecnica.repository.database.entities.Users

class UsersRecyclerAdapter(private val context: Context,val listUsers: List<Users>, private val itemClickListener:OnUserClickListener):RecyclerView.Adapter<ViewHolder<*>>(){


    interface OnUserClickListener{
        fun onpublicationsClick(id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<*> {
        return UsersvViewHolder(LayoutInflater.from(context).inflate(R.layout.list_user,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder<*>, position: Int) {
        when(holder){
            is UsersvViewHolder ->holder.bin(listUsers[position],position)
        }
    }

    override fun getItemCount(): Int {
       return listUsers.size
    }

    inner class UsersvViewHolder (itemView: View): ViewHolder<Users>(itemView) {

       val binding = ListUserBinding.bind(itemView)
        override fun bin(item: Users, position: Int) {
            binding.name.text = item.name
            binding.email.text = item.email
            binding.phone.text = item.phone
            binding.publish.setOnClickListener { itemClickListener.onpublicationsClick(item.id) }
        }

    }
}