package com.softipal.pruebatecnica.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bin(item: T, position:Int)
}