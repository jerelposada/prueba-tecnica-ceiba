package com.softipal.pruebatecnica.repository.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class Users(
    @PrimaryKey(autoGenerate = false)
    val id:Int,
    val name: String,
    val email: String,
    val phone: String
)
