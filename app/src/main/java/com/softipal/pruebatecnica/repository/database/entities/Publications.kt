package com.softipal.pruebatecnica.repository.database.entities


data class Publications(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)