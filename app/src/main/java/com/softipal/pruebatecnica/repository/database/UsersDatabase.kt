package com.softipal.pruebatecnica.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.softipal.pruebatecnica.repository.database.dao.UserDao
import com.softipal.pruebatecnica.repository.database.entities.Users

@Database(entities = [Users::class],version = 1,exportSchema = false)
abstract class UsersDatabase: RoomDatabase() {
        abstract fun userDao(): UserDao

        companion object {
            private var BD:UsersDatabase?=null
            fun getDB(context: Context):UsersDatabase{
                if(BD == null){
                        BD = Room.databaseBuilder(context,UsersDatabase::class.java,"users").build()
                }
                return BD!!
            }
        }
}