package com.softipal.pruebatecnica.repository.database.services

import com.softipal.pruebatecnica.config.Config
import com.softipal.pruebatecnica.repository.database.entities.Publications
import com.softipal.pruebatecnica.repository.database.entities.Users
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface webService {

    @GET("users")
    suspend fun getUsers(): Response<List<Users>>


    @GET("posts?")
    suspend fun getPostByUser(@Query("userId") id:Int ): Response<List<Publications>>

    companion object {

        private var BASE_URL = Config.getApkBaseRoute()

        fun create() : webService {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(webService::class.java)

        }
    }
}