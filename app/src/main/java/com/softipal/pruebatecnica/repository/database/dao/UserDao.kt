package com.softipal.pruebatecnica.repository.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.softipal.pruebatecnica.repository.database.entities.Users

@Dao
interface UserDao {

    @Query("select * from users")
    fun  findAll(): List<Users>

    @Insert
    fun store(users: Users)


    @Query("select * from users where id = :userId")
    fun getUserById(userId: Int): Users


    @Query("select * from users where name Like '%'|| :search || '%'")
    fun getUsers(search: String): List<Users>

}