package com.softipal.pruebatecnica.config

object Config {

    // Api service
    private const val apkRoute= "https://jsonplaceholder.typicode.com/"


    fun getApkBaseRoute(): String{
        return  apkRoute
    }

}