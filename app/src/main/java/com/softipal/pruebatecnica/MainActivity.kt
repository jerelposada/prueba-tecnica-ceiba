package com.softipal.pruebatecnica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.softipal.pruebatecnica.Aplication.App
import com.softipal.pruebatecnica.databinding.ActivityMainBinding
import com.softipal.pruebatecnica.repository.database.services.webService
import com.softipal.pruebatecnica.repository.database.entities.Users
import com.softipal.pruebatecnica.ui.UsersRecyclerAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity(),UsersRecyclerAdapter.OnUserClickListener, TextWatcher {
    private lateinit var binding: ActivityMainBinding
    private lateinit var searchCriterion: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.search.addTextChangedListener(this)
        lifecycleScope.launch {
            withContext(Dispatchers.IO){
              var listUser: List<Users> = App.getDB().userDao().findAll()

                if(listUser.isEmpty()){
                    val response = webService.create().getUsers();
                    if(!response.isSuccessful()){
                        Log.d("myTag", "hubo un problema")
                        Log.d("myTag", response.code().toString())
                    }
                    val Users : List<Users>? = response.body()
                    for (User in Users!!){
                     App.getDB().userDao().store(User)
                    }
                    listUser = response.body()!!

                    Log.d("myTag", response.body().toString())
                    Log.d("myTag", "llego la respuesta bien")
                }else{
                    Log.d("myTag", "ya hay datos en la base de datos")
                }


                setupRecyclerView(listUser)
            }
        }
    }

    private fun setupRecyclerView(listUsers: List<Users>){

        val reciclerView : RecyclerView = binding.UsersRecyclerView
        reciclerView.layoutManager= LinearLayoutManager(this)

        reciclerView.adapter = UsersRecyclerAdapter(this,listUsers,this)

    }

    override fun onpublicationsClick(id: Int) {
        val intent = Intent(this,CommentsActivity::class.java)
        intent.putExtra("idUSer",id)
        startActivity(intent)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(searchs: CharSequence?, p1: Int, p2: Int, p3: Int) {
        searchCriterion = searchs.toString()
        var listUser: List<Users>
        lifecycleScope.launch {
            withContext(Dispatchers.IO){
                 listUser = App.getDB().userDao().getUsers(searchCriterion)
            }

            if(listUser.isEmpty()){
                binding.textmessage.setVisibility(VISIBLE);
            }else{
                binding.textmessage.setVisibility(GONE);
            }
            setupRecyclerView(listUser)
        }
    }

    override fun afterTextChanged(p0: Editable?) {

    }
}