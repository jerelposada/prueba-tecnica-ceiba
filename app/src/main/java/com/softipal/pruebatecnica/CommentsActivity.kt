package com.softipal.pruebatecnica

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.softipal.pruebatecnica.Aplication.App
import com.softipal.pruebatecnica.databinding.ActivityCommentsBinding
import com.softipal.pruebatecnica.databinding.ActivityMainBinding
import com.softipal.pruebatecnica.repository.database.entities.Publications
import com.softipal.pruebatecnica.repository.database.entities.Users
import com.softipal.pruebatecnica.repository.database.services.webService
import com.softipal.pruebatecnica.ui.PublicationRecyclerAdapter
import com.softipal.pruebatecnica.ui.UsersRecyclerAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
class CommentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCommentsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCommentsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val userID = intent.getIntExtra("idUSer", 1)
        var listPublications: List<Publications>?= null
        lifecycleScope.launch {
            withContext(Dispatchers.IO){
                    var User = App.getDB().userDao().getUserById(userID)
                    assignUserValuesToScreen(User)
                    val response = webService.create().getPostByUser(userID)
                    if(response.isSuccessful()){
                        listPublications = response.body()
                    }
            }
            setupRecyclerView(listPublications!!)
        }

    }

    private fun assignUserValuesToScreen(user: Users){
        binding.name.text = user.name
        binding.email.text = user.email
        binding.phone.text = user.phone
    }

    private fun setupRecyclerView(listPublications: List<Publications>){

        val reciclerView : RecyclerView = binding.publicationRecyclerView
        reciclerView.layoutManager= LinearLayoutManager(this)

       reciclerView.adapter = PublicationRecyclerAdapter(this,listPublications)

    }
}